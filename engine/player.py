import json


class Player:

    def __init__(self, nickname, json_file=None):
        with open(json_file, 'r') as infile:
            self.game_stats = json.load(infile)

        self.json_file = json_file
        self.nickname = nickname
        self.stats = self.game_stats[nickname]

    def __str__(self):
        return self.nickname

    def __eq__(self, other):
        return (self.nickname == other.nickname and
                self.json_file == other.json_file)

    def wins(self):
        wins = 0
        if self.stats is not None:
            for win_ratio in self.stats.values():
                wins += win_ratio[0]
        return wins

    def loses(self):
        loses = 0
        if self.stats is not None:
            for win_ratio in self.stats.values():
                loses += win_ratio[1]
        return loses

    def win_ratio_vs(self, other):
        if isinstance(other, Player):
            if self.stats is not None:
                if str(other) in self.stats.keys():
                    return self.stats[str(other)]
        return [0, 0]

    def games_played(self):
        return self.wins() + self.loses()
