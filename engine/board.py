from config import *
from copy import deepcopy


class MustTakeFromOneRow(Exception):
    pass


class MatchsticksNotConnected(Exception):
    pass


class ItemNonExistent(Exception):
    pass


class MatchsticksGameBoard:

    def __init__(self, board_type):
        self.board = deepcopy(BOARD_TYPES[board_type])

    def __str__(self):
        board = ''
        board_width = 2 * max([len(row) for row in self.board])
        for row in self.board:
            board += (' '.join(str(x) for x in row)).center(board_width) + '\n'
        return board

    # def __repr__(self):
    #     return self.board

    def __setitem__(self, indexes, value):
        self.__check_valid_input(indexes)
        for index in indexes:
            row, column = index
            self.board[row - 1][column - 1] = value

    def __getitem__(self, index):
        row, column = index
        return self.board[row - 1][column - 1]

    def empty(self):
        return set(sum(self.board, [])) == set([0])

    def remove_matches(self, *indexes):
        self[indexes] = 0

    def __check_valid_input(self, indexes):
        row_indexs = set()
        column_indexs = []
        for index in indexes:
            if self[index] != 1:
                raise ItemNonExistent

            row, column = index
            if row <= 0 or column <= 0:
                raise IndexError
            row_indexs.add(row)
            column_indexs.append(column)

        if len(row_indexs) != 1:
            raise MustTakeFromOneRow

        for index in column_indexs:
            if index != max(column_indexs) and index + 1 not in column_indexs:
                raise MatchsticksNotConnected
