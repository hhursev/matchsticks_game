from random import choice


class AI:

    def __init__(self, board):
        self.board = board

    def __str__(self):
        return 'computer'

    def __power_of_2(self, number):
        return number != 0 and ((number & (number - 1)) == 0)

    def __stacks_of_matches(self):
        stacks = []
        for row in self.board.board:
            connected_matches = 0
            for item in row:
                if item == 1:
                    connected_matches += 1
                    continue
                stacks.append(connected_matches)
                connected_matches = 0
            stacks.append(connected_matches)
        return [stack for stack in stacks if stack != 0]

    def __break_in_power_of_twos(self):
        power_of_twos = []
        for stack in self.__stacks_of_matches():
            for number in range(stack + 1, 0, -1):
                if stack == 1:
                    power_of_twos.append(1)
                    break
                if self.__power_of_2(number) and number <= stack:
                    stack = stack % number
                    power_of_twos.append(number)
        return power_of_twos

    def __nim_sum(self):
        nims = {}
        for number in self.__break_in_power_of_twos():
            nims[number] = self.__break_in_power_of_twos().count(number)
        return {nim: nims[nim] % 2 for nim in nims if nims[nim] % 2 != 0}

    def __nim_sum_zero(self):
        return len(self.__nim_sum()) == 0

    def __max_stack_on_row(self, row):
        stacks = []
        counter = 0
        for elem in row:
            if elem == 1:
                counter += 1
                continue
            stacks.append(counter)
            counter = 0
        stacks.append(counter)
        return max(stacks)

    def __take_this_count_of_matches(self, number):
        for row in self.board.board[::-1]:
            if max(self.__stacks_of_matches()) != self.__max_stack_on_row(row):
                continue
            for index, item in enumerate(row):
                if row[index:index + number] == [1 for i in range(number)]:
                    return (len(row), index, number)

    def __take_correct_matches(self):
        stacks_in_descending_order = self.__stacks_of_matches()
        stacks_in_descending_order.sort(reverse=True)

        for number in range(max(stacks_in_descending_order), 0, -1):
            for row in self.board.board[::-1]:
                for index in range(len(row)):
                    if (set(row[index:number + index]) == {1} and
                            number + index <= len(row)):
                        row[index:number + index] = [0 for i in range(number)]
                        if self.__nim_sum_zero():
                            row[index:number + index] = [
                                1 for i in range(number)]
                            return(len(row), index, number)
                        else:
                            row[index:number + index] = [
                                1 for i in range(number)]

    def __take_random_matches(self):
        while True:
            number = choice([num for num in self.__stacks_of_matches()])
            row = choice(self.board.board)
            if row.count(1) < number:
                continue
            else:
                index = choice(list(range(len(row))))
                if row[index:number + index] == [1 for i in range(number)]:
                    return (len(row), index, number)
                continue

    def __only_single_matches(self):
        return len(self.__stacks_of_matches()) ==\
            self.__stacks_of_matches().count(1)

    def __stacks_with_singles_and_big_odd(self):
        return (len(self.__stacks_of_matches()) % 2 == 1 and
                self.__stacks_of_matches().count(1) ==
                len(self.__stacks_of_matches()) - 1)

    def __stacks_with_singles_and_big_even(self):
        return (len(self.__stacks_of_matches()) % 2 == 0 and
                self.__stacks_of_matches().count(1) ==
                len(self.__stacks_of_matches()) - 1)

    def __move_to_str(self, move):
        move_to_str = ''
        row_number = len(self.board.board)
        for row in self.board.board[::-1]:
            if len(row) == move[0] and row.count(1) >= move[2] - move[1]:
                break
            else:
                row_number -= 1
        for index in range(1, move[2]+1):
            move_to_str += "{} {} ".format(row_number, move[1] + index)
        return move_to_str

    def make_move(self):
        move = None

        if self.__only_single_matches():
            move = self.__take_this_count_of_matches(1)
        elif self.__stacks_with_singles_and_big_odd() and move is None:
            move = self.__take_this_count_of_matches(
                max(self.__stacks_of_matches()) - 1)
        elif self.__stacks_with_singles_and_big_even() and move is None:
            move = self.__take_this_count_of_matches(
                max(self.__stacks_of_matches()))
        elif move is None:
            move = self.__take_correct_matches()

        if move is None:
            move = self.__take_random_matches()

        return self.__move_to_str(move)
