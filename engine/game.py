import re
from copy import deepcopy
from board import *
from player import *
from os import path


class NicknameTaken(Exception):
    pass


class MatchsticksGame:

    def __init__(self, board, player_1=None, player_2=None, json_file=None):
        if json_file is not None and path.getsize(json_file) > 0:
            with open(json_file, 'r') as infile:
                self.players_container = json.load(infile)
                self.json_file = json_file
        self.board = board
        self.player_1 = player_1
        self.player_2 = player_2
        self.on_turn_is = [player_1, player_2]

    def __str__(self):
        return self.board.__str__()

    def on_move_is(self):
        return self.on_turn_is[0]

    def make_move(self, *move):
        try:
            self.board.remove_matches(*move)
        except (MustTakeFromOneRow, MatchsticksNotConnected):
            self.on_turn_is.reverse()
            raise
        except IndexError:
            print("You're touching places out of the board, pervert")
            self.on_turn_is.reverse()
        except ItemNonExistent:
            print("These matchsticks are already taken man...")
            self.on_turn_is.reverse()
        finally:
            self.on_turn_is.reverse()

    def winner(self):
        if self.is_game_over():
            return str(self.on_turn_is[0])

    def loser(self):
        if self.is_game_over():
            return str(self.on_turn_is[1])

    def is_game_over(self):
        return self.board.empty()

    def update_results(self):
        return self.update_players_container(self.winner(), self.loser())

    def create_player(self, nickname):
        if nickname in self.players_container:
            raise NicknameTaken
        else:
            self.players_container[nickname] = None
            self.update_players_container()

    def update_json_file(self):
        self.update_results()
        with open(self.json_file, 'w') as outfile:
            json.dump(self.players_container, outfile, sort_keys=True,
                      indent=4, separators=(',', ': '))

    def all_registered_players(self):
        return [Player(player, self.json_file) for player in
                self.players_container.keys()]

    def players_wins(self):
        return {str(player): player.wins() for player in
                self.all_registered_players()}

    def sorted_tuples_of_best_players(self):
        wins = sorted(self.players_wins().values())
        players = list(self.players_wins().keys())
        nickname_win = []
        while len(players):
            for player in players:
                if self.players_wins()[player] == wins[-1]:
                    nickname_win.append((player, wins[-1]))
                    players.remove(player)
                    wins = wins[0:-1]
        return nickname_win

    def rankings(self):
        rankings = []
        indx = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
        for nickname_win in self.sorted_tuples_of_best_players()[:9]:
            width_30_chars = (indx[0] + '. ' + nickname_win[0] + ' with ' +
                              str(nickname_win[1]) + ' wins').ljust(30)
            indx = indx[1:]
            rankings.append(width_30_chars)
        return rankings

    def favourite(self):
        favourite = self.player_1.win_ratio_vs(self.player_2)
        if favourite[0] == favourite[1]:
            return "Both players are equally matched"
        elif favourite[0] > favourite[1]:
            return '{} is favourite'.format(self.player_1.__str__())
        else:
            return '{} is favourite'.format(self.player_2.__str__())

    def parser(self, given_string):
        pattern = re.compile("([0-9]+ [0-9]+)")
        big_tuple = ()
        all_in_the_string = re.findall(pattern, given_string)
        for string in all_in_the_string:
            small_tuple = (int(string.split(" ")[0]), int(string.split(" ")[1]))
            big_tuple = big_tuple + (small_tuple,)
        return big_tuple

    def update_players_container(self, winner=None, loser=None):
        if winner is not None and loser is not None:
            try:
                self.players_container[loser][winner][1] += 1
                self.players_container[winner][loser][0] += 1
            except:
                pass
            finally:
                if self.players_container[winner] is None:
                    self.players_container.update({winner: {loser: [1, 0]}})
                if self.players_container[loser] is None:
                    self.players_container.update({loser: {winner: [0, 1]}})
                if loser not in self.players_container[winner]:
                    self.players_container[winner][loser] = [1, 0]
                if winner not in self.players_container[loser]:
                    self.players_container[loser][winner] = [0, 1]

        updated = deepcopy(self.players_container)

        for item in self.players_container:
            if self.players_container[item] is not None:
                for key in self.players_container[item]:
                    if key not in self.players_container:
                        updated.update({key: {item: self.players_container[
                            item][key][::-1]}})
                    else:
                        if (sum(updated[key][item]) <
                                sum(self.players_container[item][key])):
                            updated[key][item] = self.players_container[
                                item][key][::-1]
                        else:
                            if (sum(updated[key][item]) >
                                    sum(self.players_container[item][key])):
                                self.players_container[item][
                                    key] = updated[key][item][::-1]

        for item in self.players_container:
            if self.players_container[item] is not None:
                for key in self.players_container[item]:
                    if item not in updated[key]:
                        updated[key][item] = self.players_container[
                            item][key][::-1]

        self.players_container = deepcopy(updated)
        return updated
