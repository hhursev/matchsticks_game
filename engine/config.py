import pygame


WINDOW_SIZE = (750, 600)

JSON_FILE = './engine/players/new_json.json'

IMAGE = {
    'background': pygame.image.load('./assets/background.png'),
    'matchsticks_title': pygame.image.load(
        './assets/matchsticks_title.png'),
    'play': pygame.image.load('./assets/play.png'),
    'register': pygame.image.load('./assets/register.png'),
    'exit': pygame.image.load('./assets/exit.png'),
    'hover_exit': pygame.image.load('./assets/hover_exit.png'),
    'clicked_exit': pygame.image.load('./assets/clicked_exit.png'),
    'hover_play': pygame.image.load('./assets/hover_play.png'),
    'hover_register': pygame.image.load(
    './assets/hover_register.png'),
    'clicked_play': pygame.image.load('./assets/clicked_play.png'),
    'clicked_register': pygame.image.load('./assets/clicked_register.png'),
    'sign_up': pygame.image.load('./assets/sign_up.png'),
    'hover_sign_up': pygame.image.load('./assets/sign_up_hover.png'),
    'clicked_sign_up': pygame.image.load('./assets/sign_up_clicked.png'),
    'main_menu': pygame.image.load('./assets/main_menu.png'),
    'hover_main_menu': pygame.image.load('./assets/main_menu_hover.png'),
    'clicked_main_menu': pygame.image.load('./assets/main_menu_clicked.png'),
    'rankings_background': pygame.image.load(
        './assets/rankings_background.png'),
    'choose_game': pygame.image.load('./assets/choose_game.png'),
    'rankings_seperator': pygame.image.load('./assets/rankings_seperator.png'),
    'play_vs_fr': pygame.image.load('./assets/play_vs_friend.png'),
    'hover_play_vs_fr': pygame.image.load('./assets/hover_play_vs_friend.png'),
    'clicked_play_vs_fr': pygame.image.load(
    './assets/clicked_play_vs_friend.png'),
    'play_vs_computer': pygame.image.load(
        './assets/play_vs_computer.png'),
    'hover_play_vs_computer': pygame.image.load(
    './assets/hover_play_vs_computer.png'),
    'clicked_play_vs_computer': pygame.image.load(
        './assets/clicked_play_vs_computer.png'),
    'main_menu_game': pygame.image.load('./assets/main_menu.png'),
    'hover_main_menu_game': pygame.image.load('./assets/main_menu_hover.png'),
    'clicked_main_menu_game': pygame.image.load(
    './assets/main_menu_clicked.png'),
    'player_1': pygame.image.load('./assets/player_1.png'),
    'hover_player_1': pygame.image.load('./assets/hover_player_1.png'),
    'clicked_player_1': pygame.image.load('./assets/clicked_player_1.png'),
    'player_2': pygame.image.load('./assets/player_2.png'),
    'hover_player_2': pygame.image.load('./assets/hover_player_1.png'),
    'clicked_player_2': pygame.image.load('./assets/clicked_player_1.png'),
    'sign_up_background': pygame.image.load('./assets/sign_up_background.png'),
    'sign_in': pygame.image.load('./assets/sign_in.png'),
    'hover_sign_in': pygame.image.load('./assets/hover_sign_in.png'),
    'clicked_sign_in': pygame.image.load('./assets/clicked_sign_in.png'),
    'matchstick': pygame.image.load('./assets/matchstick.png'),
    'selected_matchstick': pygame.image.load(
        './assets/selected_matchstick.png'),
    'submit': pygame.image.load('./assets/submit.png'),
    'hover_submit': pygame.image.load('./assets/hover_submit.png'),
    'clicked_submit': pygame.image.load('./assets/clicked_submit.png'),
    'quit': pygame.image.load('./assets/quit.png'),
    'hover_quit': pygame.image.load('./assets/hover_quit.png'),
    'clicked_quit': pygame.image.load('./assets/clicked_quit.png'),
    'trophy': pygame.image.load('./assets/trophy.png'),
    'reset_move': pygame.image.load('./assets/reset_move.png'),
    'hover_reset_move': pygame.image.load('./assets/hover_reset_move.png'),
    'clicked_reset_move': pygame.image.load('./assets/clicked_reset_move.png')
}

POSITION = {
    'play': (200, 200),
    'register': (200, 280),
    'exit': (200, 360),
    'sign_up': (200, 350),
    'main_menu': (400, 350),
    'play_vs_fr': (20, 230),
    'play_vs_computer': (20, 300),
    'main_menu_game': (10, 520),
    'player_1': (20, 380),
    'player_2': (20, 450),
    'sign_in': (280, 350),
    'submit': (380, 480),
    'quit': (540, 480),
    'reset_move': (430, 540)
}

SIZE = {
    'matchstick': (20, 100)
}

BOARD_TYPES = {
    1: [[1], [1, 1], [1, 1, 1], [1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1]],
    2: [[1], [1, 1, 1], [1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1]],
    3: [[1], [1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1]],
    4: [[1], [1, 1, 1, 1], [1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1]],
    5: [[1], [1, 1], [1, 1, 1, 1], [1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1]],
    6: [[1, 1], [1, 1], [1, 1, 1], [1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1]],
    7: [[1, 1], [1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1]],
    8: [[1], [1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1, 1, 1]],
    9: [[1], [1, 1, 1], [1, 1, 1, 1, 1], [1, 1, 1, 1]]
}

button_states = {'play': '', 'register': '', 'exit': '', 'sign_up': '',
                 'main_menu': '', 'play_vs_computer': '', 'play_vs_fr': '',
                 'main_menu_game': '', 'player_1': '', 'player_2': '',
                 'sign_in': '', 'submit': '', 'quit': '', 'reset_move': ''}
