import sys
import pygame
from random import shuffle

sys.path.append('./engine')

from board import *
from game import *
from AI import *
from config import *


class Button:

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return self.name

    def get_area(self):
        area = IMAGE['{}{}'.format(
            button_states[self.name], self.name)].get_rect()  # image size
        area[0] = POSITION[self.name][0]  # width offset
        area[1] = POSITION[self.name][1]  # height offset
        return area


class Game:

    def __init__(self, size):
        board = MatchsticksGameBoard(1)
        self.json = JSON_FILE
        self.game = MatchsticksGame(board, None, None, self.json)
        self.create_computer_if_none()
        self.players = []

        self.screen = pygame.display.set_mode(size)
        self.running = True
        self.state = 'intro'
        self.previous_state = ''
        self.action = ''
        self.buttons = self.initialize_buttons()
        self.left_key_pressed = False

        self.nickname = ''
        self.error_text = 'enter your nickname'

        self.rankings_height = -500
        self.rankings_fall_speed = 15

        self.player_1 = ''
        self.player_2 = ''
        self.player = ''
        self.to_sign_in = None
        self.game_start_errors = ''
        self.computer_player = False

        self.matchstick_positions = {}
        self.mouse_hits = []
        self.move = ''
        self.move_errors = ''
        self.winner = None
        self.submit_winner = False

    def create_computer_if_none(self):
        if self.json is not None and path.getsize(self.json) > 0:
            with open(self.json, 'r') as infile:
                is_there_computer = json.load(infile)
        if 'computer' not in is_there_computer.keys():
            self.nickname = 'computer'
            self.try_to_create_player()
            self.nickname = ''

    def intro_handler(self, event):
        self.buttons_handler(event, 'play', 'register', 'exit')
        self.escape_key(event)

    def intro_display(self):
        self.screen.blit(IMAGE['background'], (0, 0))
        self.screen.blit(IMAGE['matchsticks_title'], (95, 10))
        self.buttons_display('play', 'register', 'exit')

    def buttons_display(self, *buttons):
        for button in buttons:
            self.screen.blit(IMAGE['{}{}'.format(
                button_states[button], button)], POSITION[button])

    def buttons_handler(self, event, *buttons):
        mouse_pos = pygame.mouse.get_pos()
        left_key = pygame.mouse.get_pressed()[0]
        for button in buttons:
            button_area = self.buttons[button].get_area()  # easier on the eyes
            if (event.type == pygame.MOUSEBUTTONUP and self.left_key_pressed
                    and button_area.collidepoint(mouse_pos)):
                self.action = button
                self.left_key_pressed = False
            elif left_key and button_area.collidepoint(mouse_pos):
                self.left_key_pressed = True
                button_states[button] = 'clicked_'
            elif self.buttons[button].get_area().collidepoint(mouse_pos):
                button_states[button] = 'hover_'
            else:
                button_states[button] = ''

        if self.action == 'exit':
            self.running = False
        elif self.action == 'main_menu' or self.action == 'main_menu_game':
            self.previous_state, self.state = self.state, 'intro'
            self.player_1 = ''
            self.player_2 = ''
            self.game_start_errors = ''
        elif self.action == 'register':
            self.previous_state, self.state = self.state, 'register'
        elif self.action == 'sign_up':
            self.try_to_create_player()
        elif self.action == 'play':
            self.previous_state, self.state = self.state, 'game'
        elif self.action == 'player_1' or self.action == 'player_2':
            self.previous_state, self.state = self.state, 'sign_up'
            if self.action == 'player_1':
                self.to_sign_in = 1
            else:
                self.to_sign_in = 2
            self.game_start_errors = ''

        elif self.action == 'play_vs_computer' or self.action == 'play_vs_fr':
            self.game_start_errors = ''
            if self.action == 'play_vs_fr':
                if self.player_1 == '' or self.player_2 == '':
                    self.game_start_errors = 'Two players needed!'
            elif self.action == 'play_vs_computer':
                if self.player_1 == '' and self.player_2 == '':
                    self.game_start_errors = 'Input at least one user!'

            if self.game_start_errors == '':
                if self.action == 'play_vs_computer':
                    self.computer_player = True
                self.initialize_game()
                self.previous_state, self.state = self.state, 'play'

        elif self.action == 'sign_in':
            if self.player == 'computer':
                self.error_text = "you can't be computer"
            elif self.player not in self.game.players_container.keys():
                self.error_text = 'non existent account'
            else:
                if self.to_sign_in == 1:
                    self.player_1 = self.player
                else:
                    self.player_2 = self.player
                self.player = ''
                self.error_text = 'enter your nickname'
                self.to_sign_in = None
                self.previous_state, self.state = self.state, 'game'

        elif self.action == 'submit':
            self.submit_move()

        elif self.action == 'quit':
            self.state, self.previous_state = 'game', 'intro'
            self.player_1, self.player_2 = '', ''
            self.move_errors = ''
            self.computer_player = False

        elif self.action == 'reset_move':
            self.mouse_hits = []

        button_states[self.action] = ''
        self.action = ''

    def initialize_game(self):
        board = MatchsticksGameBoard(choice(range(1, len(BOARD_TYPES) + 1)))
        if self.computer_player:
            if self.player_1 == '':
                self.players = [Player('computer', self.json),
                                Player(self.player_2, self.json)]
            else:
                self.players = [Player('computer', self.json),
                                Player(self.player_1, self.json)]
                shuffle(self.players)
            self.game = MatchsticksGame(board, self.players[0],
                                        self.players[1], self.json)
        else:
            self.players = [Player(self.player_1, self.json),
                            Player(self.player_2, self.json)]
            shuffle(self.players)
            self.game = MatchsticksGame(board, self.players[0],
                                        self.players[1], self.json)
        self.winner = None
        self.submit_winner = False

    def try_to_create_player(self):
        if len(self.nickname) < 6:
            self.error_text = 'nickname yet too short'
        else:
            try:
                self.game.create_player(self.nickname)
            except NicknameTaken:
                self.error_text = 'nickname already taken'
            else:
                self.game.update_json_file()
                self.error_text = 'create one more if you wish'
                self.nickname = ''
            finally:
                self.action = ''

    def register_handler(self, event):
        self.buttons_handler(event, 'sign_up', 'main_menu')
        self.escape_key(event)
        self.input_nickname(event)

    def draw_text(self, text, font, color, position, pos_x=None, pos_y=None,
                  offset_x=None, offset_y=None):
        block = font.render(text, True, color)
        if position == 'center':
            position = block.get_rect()
            position.center = self.screen.get_rect().center
        if pos_x is not None:
            position[0] = pos_x
        if pos_y is not None:
            position[1] = pos_y
        if offset_x is not None:
            position[0] += offset_x
        if offset_y is not None:
            position[1] += offset_y
        return self.screen.blit(block, position)

    def register_display(self):
        self.screen.blit(IMAGE['background'], (0, 0))
        nickname_font = pygame.font.Font(None, 50)
        errors_font = pygame.font.Font(None, 20)
        self.draw_text(self.nickname, nickname_font, (200, 225, 225), 'center')
        self.draw_text(self.error_text, errors_font, (175, 0, 0),
                       'center', None, 330)
        self.buttons_display('sign_up', 'main_menu')

    def escape_key(self, event):
        if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            if self.previous_state is not '':
                self.state, self.previous_state =\
                    self.previous_state, self.state
                self.player_1 = ''
                self.player_2 = ''
                self.game_start_errors = ''
                self.computer_player = False
            else:
                self.running = False

    def input_nickname(self, event):
        if event.type == pygame.KEYDOWN:
            self.error_text = ''
            if (event.unicode.isalpha() or event.unicode.isdigit() or
               event.unicode == '_'):
                if len(self.nickname) > 11:
                    self.error_text = 'maximum length reached'
                else:
                    self.nickname += event.unicode
            elif event.key == pygame.K_BACKSPACE:
                if len(self.nickname) == 1 or len(self.nickname) == 0:
                    self.error_text = 'enter your nickname'
                self.nickname = self.nickname[:-1]
            elif event.key == pygame.K_RETURN:
                self.action = 'sign_up'
            elif event.key == pygame.K_RSHIFT or event.key == pygame.K_LSHIFT:
                self.error_text = ''
            else:
                self.error_text = 'only digits and alphas allowed'

    def initialize_buttons(self):
        return {button: Button(button) for button in button_states.keys()}

    def game_handler(self, event):
        self.escape_key(event)
        self.buttons_handler(event, 'play_vs_fr', 'play_vs_computer',
                             'main_menu_game', 'player_1', 'player_2')

    def game_display(self):
        self.screen.blit(IMAGE['background'], (0, 0))
        self.screen.blit(IMAGE['choose_game'], (20, 20))
        self.buttons_display('play_vs_fr', 'play_vs_computer',
                             'main_menu_game', 'player_1', 'player_2')
        players_font = pygame.font.Font(None, 35)
        self.draw_text(self.player_1, players_font, (200, 135, 65), (150, 395))
        self.draw_text(self.player_2, players_font, (200, 135, 65), (150, 465))

        errors_font = pygame.font.Font(None, 25)
        if self.game_start_errors == 'Two players needed!':
            self.draw_text(self.game_start_errors, errors_font, (175, 0, 0),
                           'center', None, 295, -220)
        elif self.game_start_errors == 'Input at least one user!':
            self.draw_text(self.game_start_errors, errors_font, (175, 0, 0),
                           'center', None, 360, -220)

        self.rankings()

    def rankings(self):
        rankings = self.screen.blit(IMAGE['rankings_background'],
                                    (330, self.rankings_height))

        heading_font = pygame.font.Font(None, 50)
        rankings_font = pygame.font.Font(None, 30)

        if self.rankings_height < 50:
            self.rankings_height += self.rankings_fall_speed
            self.rankings_fall_speed -= 0.2
        else:
            self.draw_text('Rankings List', heading_font, (191, 127, 63),
                           (rankings[0] + 80, self.rankings_height + 30))
            self.screen.blit(IMAGE['rankings_seperator'], (rankings[0] + 15,
                             self.rankings_height + 80))

            offsets = [160, 190, 220, 250, 280, 310, 340, 370, 400, 420]
            for top_player in self.game.rankings():
                self.draw_text(top_player, rankings_font, (0, 200, 200),
                               (rankings[0] + 40, rankings[1] + offsets[0]))
                offsets.pop(False)

            self.screen.blit(IMAGE['rankings_seperator'], (rankings[0] + 15,
                             rankings[1] + offsets[-1]))

    def sign_up_player(self, event):
        if event.type == pygame.KEYDOWN:
            self.error_text = ''
            if (event.unicode.isalpha() or event.unicode.isdigit() or
               event.unicode == '_'):
                if len(self.player) > 11:
                    self.error_text = 'maximum length reached'
                else:
                    self.player += event.unicode
            elif event.key == pygame.K_BACKSPACE:
                if len(self.player) == 1 or len(self.player) == 0:
                    self.error_text = 'enter your nickname'
                self.player = self.player[:-1]
            elif event.key == pygame.K_RETURN:
                self.action = 'sign_in'
            elif event.key == pygame.K_RSHIFT or event.key == pygame.K_LSHIFT:
                self.error_text = ''
            else:
                self.error_text = 'only digits and alphas allowed'

    def sign_up_handler(self, event):
        self.buttons_handler(event, 'sign_in')
        self.sign_up_player(event)
        self.escape_key(event)

    def sign_up_display(self):
        self.screen.blit(IMAGE['sign_up_background'], (0, 0))
        nickname_font = pygame.font.Font(None, 50)
        self.draw_text(self.player, nickname_font, (200, 225, 225), 'center')
        errors_font = pygame.font.Font(None, 20)
        self.draw_text(self.error_text, errors_font, (175, 0, 0), 'center',
                       None, 330)
        self.buttons_display('sign_in')

    def play_handler(self, event):
        if self.game.is_game_over() is False:
            if self.game.on_turn_is[0].__str__() == 'computer':
                self.computer_turn()
            else:
                self.human_turn(event)
        elif self.winner.nickname != '' and self.submit_winner is False:
            self.game.update_json_file()
            self.submit_winner = True
        elif self.winner.nickname != '':
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_F5:
                    self.initialize_game()
        self.escape_key(event)
        self.buttons_handler(event, 'submit', 'quit', 'reset_move')

    def play_display(self):
        self.screen.blit(IMAGE['background'], (0, 0))
        self.matchsticks_board()

        opponents_font = pygame.font.Font(None, 60)
        favourite_font = pygame.font.Font(None, 35)
        on_turn_font = pygame.font.Font(None, 45)
        rmach_font = pygame.font.Font(None, 30)
        error_font = pygame.font.Font(None, 25)
        opponents = "{} vs {}".format(self.game.player_1, self.game.player_2)
        on_turn = self.game.on_turn_is[0].__str__()

        self.draw_text(opponents, opponents_font, (105, 100, 80), 'center',
                       None, 40, 120)
        self.draw_text(self.game.favourite(), favourite_font, (180, 180, 50),
                       'center', None, 100, 120)

        if self.game.is_game_over():
            self.winner = self.game.on_turn_is[0]
            self.screen.blit(IMAGE['trophy'], (350, 200))
            self.draw_text(
                'Press F5 for a remach', rmach_font, (150, 150, 150),
                (50, 200))
            self.draw_text("{}'s trophy".format(on_turn), on_turn_font,
                           (0, 150, 0), 'center', None, 150, 120)
        else:
            self.draw_text("{}'s turn".format(on_turn), on_turn_font,
                           (0, 150, 0), 'center', None, 150, 120)

        if self.move_errors != '':
            error_text = "ERROR: {}".format(self.move_errors)
            self.draw_text(error_text, error_font, (200, 30, 30),
                           'center', None, 180, 120)

        self.buttons_display('submit', 'quit', 'reset_move')

    def matchsticks_board(self):
        elem_y, elem_x = 30, 20
        row, column = 1, 1
        for elem in self.game.__str__():
            if elem == '\n':
                elem_y, elem_x = elem_y + 110, 20
                row, column = row + 1, 1
            elif elem == ' ':
                elem_x += 20
            elif elem == '0':
                elem_x, column = elem_x + 20, column + 1
            elif elem == '1':
                self.screen.blit(IMAGE['matchstick'], (elem_x, elem_y))
                self.matchstick_positions[(elem_x, elem_y)] = "{} {} ".format(
                    row, column)
                column, elem_x = column + 1, elem_x + 20

        for elem in self.mouse_hits:
            self.screen.blit(IMAGE['selected_matchstick'], elem)

    def human_turn(self, event):
        if event.type == pygame.MOUSEBUTTONUP:
            self.select_matches(pygame.mouse.get_pos())
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                self.submit_move()
            elif event.key == pygame.K_ESCAPE:
                self.mouse_hits = []
            elif event.key == pygame.K_BACKSPACE:
                if len(self.mouse_hits) > 0:
                    self.mouse_hits.pop()

    def select_matches(self, mouse_position):
        if self.is_selection(mouse_position)[0]:
            if self.is_selection(mouse_position)[1] in self.mouse_hits:
                self.mouse_hits.remove(self.is_selection(mouse_position)[1])
            else:
                self.mouse_hits.append(self.is_selection(mouse_position)[1])

    def is_selection(self, coordinates):
        for position in self.matchstick_positions.keys():
            if (position[0] <= coordinates[0] and
                position[0] + SIZE['matchstick'][0] >= coordinates[0] and
                position[1] <= coordinates[1] and
               position[1] + SIZE['matchstick'][1] >= coordinates[1]):
                return (True, position)
            else:
                continue
        return (False, )

    def submit_move(self):
        move = ''
        for hit in self.mouse_hits:
            move += self.matchstick_positions[hit]
        self.update_board(move)

    def update_board(self, move):
        try:
            self.game.make_move(*self.game.parser(move))
        except MustTakeFromOneRow:
            self.move_errors = 'You must take from one row'
        except MatchsticksNotConnected:
            self.move_errors = 'Matchsticks must be neighbours'
        else:
            self.mouse_hits = []
            self.matchstick_positions = {}
            self.move_errors = ''

    def computer_turn(self):
        computer = AI(self.game.board)
        taking_matches = computer.make_move()
        self.update_board(taking_matches)

    def event_handler(self, event):
        if self.state == 'intro':
            self.intro_handler(event)
        elif self.state == 'register':
            self.register_handler(event)
        elif self.state == 'game':
            self.game_handler(event)
        elif self.state == 'sign_up':
            self.sign_up_handler(event)
        elif self.state == 'play':
            self.play_handler(event)

    def display(self):
        if self.state == 'intro':
            self.intro_display()
        elif self.state == 'register':
            self.register_display()
        elif self.state == 'game':
            self.game_display()
        elif self.state == 'sign_up':
            self.sign_up_display()
        elif self.state == 'play':
            self.play_display()

    def main(self):
        clock = pygame.time.Clock()
        pygame.font.init()
        while self.running:
            clock.tick(30)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                self.event_handler(event)
            self.display()
            pygame.display.flip()

if __name__ == '__main__':
    Game(WINDOW_SIZE).main()
