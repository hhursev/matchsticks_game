Matchsticks Game
---------------------------

This is the matchsticks game. You need [Python3](http://python.org/download/) and [PyGame](http://pygame.org/download.shtml) (for Python3.2) to run it.

The game offers you:

- friendly UI
- option to play vs AI (it is unbeatable though)
- play vs friend (enemy)
- register your own username
- keeping track of your statistics (wins/games)
- personal statistics vs other players

------------------------------

Did You Know?

If you understand how the AI works and start to beat it, basically you have mastered every other [Nim type game](https://en.wikipedia.org/wiki/Nim). /wikipedia link/
