import unittest
import sys
sys.path.append('./engine')
sys.path.append('./tests')

from test_player import *
from test_board import *
from test_AI import *
from test_game import *


unittest.main()
