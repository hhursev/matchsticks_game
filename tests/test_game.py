import unittest
import game
import board
import player
import json


class MatchsticksGameTest(unittest.TestCase):

    def test_on_move_is_1(self):
        b = board.MatchsticksGameBoard(1)
        g = game.MatchsticksGame(b, 'joro', 'pepi')
        self.assertEqual('joro', g.on_move_is())

    def test_on_move_is_2(self):
        b = board.MatchsticksGameBoard(1)
        g = game.MatchsticksGame(b, 'joro', 'pepi')
        g.make_move((1, 1))
        self.assertEqual('pepi', g.on_move_is())

    def test_printing_board(self):
        b = board.MatchsticksGameBoard(1)
        g = game.MatchsticksGame(b, 'joro', 'pepi')
        board_type_1 = '       1        \n' +\
                       '      1 1       \n' +\
                       '     1 1 1      \n' +\
                       '   1 1 1 1 1    \n' +\
                       '1 1 1 1 1 1 1 1 \n'
        self.assertEqual(board_type_1, g.__str__())

    def test_is_game_over_1(self):
        b = board.MatchsticksGameBoard(1)
        g = game.MatchsticksGame(b, 'joro', 'pepi')
        g.make_move((1, 1))
        self.assertFalse(g.is_game_over())

    def test_is_game_over_2(self):
        b = board.MatchsticksGameBoard(1)
        g = game.MatchsticksGame(b, 'joro', 'pepi')
        g.make_move((1, 1))
        g.make_move((2, 1), (2, 2))
        g.make_move((3, 1), (3, 2), (3, 3))
        g.make_move((4, 1), (4, 2), (4, 3), (4, 4), (4, 5))
        g.make_move((5, 1), (5, 2), (5, 3), (5, 4),
                    (5, 5), (5, 6), (5, 7), (5, 8))
        self.assertTrue(g.is_game_over())

    def test_reading_from_json(self):
        b = board.MatchsticksGameBoard(1)
        g = game.MatchsticksGame(
            b, 'hristo', 'kamen', './tests/testing_files/player_stats_1.json')

        players_must_be = {"hristo": {"georgi": [10, 8], "kamen": [15, 2]},
                           "kamen": {"hristo": [2, 10]}, "pepa": None, "query":
                           {"georgi": [0, 5]}, "georgi": None}
        self.assertEqual(players_must_be, g.players_container)

    def test_update_players_json_1(self):
        b = board.MatchsticksGameBoard(3)
        g = game.MatchsticksGame(
            b, 'hristo', 'kamen', './tests/testing_files/player_stats_2.json')

        updated = g.update_players_container()

        with open('./tests/testing_files/modified_player_stats_2.json',
                  'r') as infile:
            must_be = json.load(infile)

        self.assertEqual(must_be, updated)

    def test_update_players_json_2(self):
        b = board.MatchsticksGameBoard(3)
        g = game.MatchsticksGame(
            b, 'hristo', 'kamen', './tests/testing_files/player_stats_3.json')

        updated = g.update_players_container()

        with open('./tests/testing_files/modified_player_stats_3.json',
                  'r') as infile:
            must_be = json.load(infile)

        self.assertEqual(must_be, updated)

    def test_update_players_json_3(self):
        b = board.MatchsticksGameBoard(3)
        g = game.MatchsticksGame(
            b, 'hristo', 'kamen', './tests/testing_files/player_stats_4.json')

        updated = g.update_players_container()

        with open('./tests/testing_files/modified_player_stats_4.json',
                  'r') as infile:
            must_be = json.load(infile)

        self.assertEqual(must_be, updated)

    def test_parser_expected_input(self):
        b = board.MatchsticksGameBoard(2)
        g = game.MatchsticksGame(b, 'hristo', 'kamen', None)
        self.assertEqual(((3, 2), (3, 3), (3, 4)),
                         g.parser("3 2 3 3 3 4 "))

    def test_parser_expected_big_input(self):
        b = board.MatchsticksGameBoard(2)
        g = game.MatchsticksGame(b, 'hristo', 'kamen', None)
        self.assertEqual(((154, 123), (154, 3), (403, 40)),
                         g.parser("154 123 154 3 403 40 "))

    def test_big_big_input(self):
        b = board.MatchsticksGameBoard(2)
        g = game.MatchsticksGame(b, 'hristo', 'kamen', None)
        self.assertEqual(((1530, 12334), (1233, 200001)),
                         g.parser("1530 12334 1233 200001"))

    def test_upgrade_jsons_after_win_of_registred_player_first_match(self):
        b = board.MatchsticksGameBoard(1)
        g = game.MatchsticksGame(
            b, 'hristo', 'kamen', './tests/testing_files/p_never_met.json')
        with open('./tests/testing_files/never_met_modified.json',
                  'r') as infile:
            must_be = json.load(infile)
        # board_is = '       1        \n' +\
        #            '      1 1       \n' +\
        #            '     1 1 1      \n' +\
        #            '   1 1 1 1 1    \n' +
        #            '1 1 1 1 1 1 1 1 \n'
        g.make_move((1, 1))
        g.make_move((2, 1), (2, 2))
        g.make_move((3, 1), (3, 2), (3, 3))
        g.make_move((4, 1), (4, 2), (4, 3), (4, 4), (4, 5))
        g.make_move((5, 1), (5, 2), (5, 3), (5, 4))
        g.make_move((5, 5), (5, 6), (5, 7), (5, 8))
        updated = g.update_results()
        self.assertEqual(must_be, updated)

    def test_upgrade_veterans_playing(self):
        b = board.MatchsticksGameBoard(1)
        g = game.MatchsticksGame(
            b, 'hristo', 'georgi', './tests/testing_files/veterans.json')
        with open('./tests/testing_files/veterans_modified.json',
                  'r') as infile:
            must_be = json.load(infile)
        # board_is = '       1        \n' +\
        #            '      1 1       \n' +\
        #            '     1 1 1      \n' +\
        #            '   1 1 1 1 1    \n' +
        #            '1 1 1 1 1 1 1 1 \n'
        g.make_move((1, 1))
        g.make_move((2, 1), (2, 2))
        g.make_move((3, 1), (3, 2), (3, 3))
        g.make_move((4, 1), (4, 2), (4, 3), (4, 4), (4, 5))
        g.make_move((5, 1), (5, 2), (5, 3), (5, 4))
        g.make_move((5, 5), (5, 6), (5, 7), (5, 8))
        updated = g.update_results()
        self.assertEqual(must_be, updated)

    def test_favourite_1(self):
        hristo = player.Player('hristo',
                               './tests/testing_files/veterans.json')
        georgi = player.Player('georgi',
                               './tests/testing_files/veterans.json')
        b = board.MatchsticksGameBoard(1)
        g = game.MatchsticksGame(
            b, hristo, georgi, './tests/testing_files/veterans.json')
        self.assertEqual('hristo is favourite', g.favourite())

    def test_favourite_2(self):
        hristo = player.Player('hristo',
                               './tests/testing_files/veterans.json')
        pepa = player.Player('pepa',
                             './tests/testing_files/veterans.json')
        b = board.MatchsticksGameBoard(1)
        g = game.MatchsticksGame(
            b, hristo, pepa, './tests/testing_files/veterans.json')
        self.assertEqual('pepa is favourite', g.favourite())

    def test_rankings_string(self):
        b = board.MatchsticksGameBoard(1)
        g = game.MatchsticksGame(
            b, 'hristo', 'pepa', './tests/testing_files/veterans.json')
        must_be = ['1. hristo with 102 wins       ',
                   '2. georgi with 40 wins        ',
                   '3. kamen with 17 wins         ',
                   '4. pepa with 1 wins           ']
        self.assertEqual(must_be, g.rankings())

    def test_players_wins(self):
        b = board.MatchsticksGameBoard(1)
        g = game.MatchsticksGame(
            b, 'hristo', 'pepa', './tests/testing_files/veterans.json')
        must_be = {'hristo': 102, 'georgi': 40, 'kamen': 17, 'pepa': 1}
        self.assertEqual(must_be, g.players_wins())

    def test_all_registered_players(self):
        b = board.MatchsticksGameBoard(1)
        g = game.MatchsticksGame(
            b, 'hristo', 'pepa', './tests/testing_files/veterans.json')
        must_be = [player.Player('kamen',
                                 './tests/testing_files/veterans.json'),
                   player.Player('hristo',
                                 './tests/testing_files/veterans.json'),
                   player.Player('georgi',
                                 './tests/testing_files/veterans.json'),
                   player.Player('pepa',
                                 './tests/testing_files/veterans.json')]
        flag = True
        for elem in must_be:
            if elem not in g.all_registered_players():
                flag = False
        self.assertTrue(flag)

    def test_error_MustTakeFromOneRow(self):
        b = board.MatchsticksGameBoard(1)
        g = game.MatchsticksGame(b, 'hristo', 'pepa', None)
        with self.assertRaises(game.MustTakeFromOneRow):
            g.make_move((2, 1), (3, 2))

    def test_error_MatchsticksNotConnected(self):
        b = board.MatchsticksGameBoard(1)
        g = game.MatchsticksGame(b, 'hristo', 'pepa', None)
        with self.assertRaises(game.MatchsticksNotConnected):
            g.make_move((3, 1), (3, 3))

    def test_sorted_tuples_of_best_players(self):
        b = board.MatchsticksGameBoard(1)
        g = game.MatchsticksGame(
            b, 'hristo', 'pepa', './tests/testing_files/veterans.json')
        must_be = [('hristo', 102), ('georgi', 40), ('kamen', 17),
                   ('pepa', 1)]
        self.assertEqual(must_be, g.sorted_tuples_of_best_players())


if __name__ == '__main__':
    unittest.main()
