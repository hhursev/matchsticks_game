import unittest
import board


class MatchsticksGameBoardTest(unittest.TestCase):

    def test_initialize_board_type_1(self):
        b = board.MatchsticksGameBoard(1)
        board_type_1 = '       1        \n' +\
                       '      1 1       \n' +\
                       '     1 1 1      \n' +\
                       '   1 1 1 1 1    \n' +\
                       '1 1 1 1 1 1 1 1 \n'

        self.assertEqual(board_type_1, b.__str__())

    def test_board_initialize_type_2(self):
        b = board.MatchsticksGameBoard(2)
        board_type_2 = '        1         \n' +\
                       '      1 1 1       \n' +\
                       '    1 1 1 1 1     \n' +\
                       '  1 1 1 1 1 1 1   \n' +\
                       '1 1 1 1 1 1 1 1 1 \n'
        self.assertEqual(board_type_2, b.__str__())

    def test_initialize_board_type_3(self):
        b = board.MatchsticksGameBoard(3)
        board_type_3 = '        1         \n' +\
                       '      1 1 1       \n' +\
                       '     1 1 1 1      \n' +\
                       '   1 1 1 1 1 1    \n' +\
                       '1 1 1 1 1 1 1 1 1 \n'

        self.assertEqual(board_type_3, b.__str__())

    def test_board_initialize_type_6(self):
        b = board.MatchsticksGameBoard(6)
        board_type_6 = '     1 1      \n' +\
                       '     1 1      \n' +\
                       '    1 1 1     \n' +\
                       ' 1 1 1 1 1 1  \n' +\
                       '1 1 1 1 1 1 1 \n'
        self.assertEqual(board_type_6, b.__str__())

    def test_initialize_board_type_9(self):
        b = board.MatchsticksGameBoard(9)
        board_type_9 = '    1     \n' +\
                       '  1 1 1   \n' +\
                       '1 1 1 1 1 \n' +\
                       ' 1 1 1 1  \n'

        self.assertEqual(board_type_9, b.__str__())

    def test_remove_matches_no_errors_1(self):
        working_board = board.MatchsticksGameBoard(1)
        board_should = '       1        \n' +\
                       '      1 1       \n' +\
                       '     0 0 1      \n' +\
                       '   1 1 1 1 1    \n' +\
                       '1 1 1 1 1 1 1 1 \n'
        working_board.remove_matches((3, 1), (3, 2))
        self.assertEqual(board_should, working_board.__str__())

    def test_remove_matches_no_errors_2(self):
        working_board = board.MatchsticksGameBoard(1)
        board_should = '       1        \n' +\
                       '      1 1       \n' +\
                       '     1 1 1      \n' +\
                       '   1 0 0 0 1    \n' +\
                       '1 1 1 1 1 1 1 1 \n'
        working_board.remove_matches((4, 2), (4, 4), (4, 3))
        self.assertEqual(board_should, working_board.__str__())

    def test_remove_matches_no_errors_3(self):
        working_board = board.MatchsticksGameBoard(1)
        board_should = '       1        \n' +\
                       '      1 1       \n' +\
                       '     0 0 0      \n' +\
                       '   1 1 1 1 1    \n' +\
                       '1 1 1 1 1 1 1 1 \n'
        working_board.remove_matches((3, 1), (3, 2), (3, 3))
        self.assertEqual(board_should, working_board.__str__())

    def test_board_empty(self):
        working_board = board.MatchsticksGameBoard(1)
        # board_should = '       1        \n' +\
        #                '      1 1       \n' +\
        #                '     1 1 1      \n' +\
        #                '   1 1 1 1 1    \n' +\
        #                '1 1 1 1 1 1 1 1 \n'

        working_board.remove_matches((3, 1), (3, 2), (3, 3))
        working_board.remove_matches((1, 1))
        working_board.remove_matches((2, 1), (2, 2))
        working_board.remove_matches((4, 1), (4, 2), (4, 3), (4, 4), (4, 5))
        working_board.remove_matches((5, 1), (5, 2), (5, 3), (5, 4), (5, 5),
                                     (5, 8), (5, 7), (5, 6))
        self.assertTrue(working_board.empty())

    def test_remove_matches_from_different_rows(self):
        invalid_take = board.MatchsticksGameBoard(1)
        # board_should = '       1        \n' +\
        #                '      1 1       \n' +\
        #                '     1 1 1      \n' +\
        #                '   1 1 1 1 1    \n' +\
        #                '1 1 1 1 1 1 1 1 \n'

        with self.assertRaises(board.MustTakeFromOneRow):
            invalid_take.remove_matches((1, 1), (2, 1))

        with self.assertRaises(board.MustTakeFromOneRow):
            invalid_take.remove_matches((3, 1), (3, 2), (4, 1))

        with self.assertRaises(board.MustTakeFromOneRow):
            invalid_take.remove_matches((4, 1), (4, 2), (4, 3), (3, 3))

    def test_remove_not_connected_matches(self):
        invalid_take = board.MatchsticksGameBoard(1)
        # board_should = '       1        \n' +\
        #                '      1 1       \n' +\
        #                '     1 1 1      \n' +\
        #                '   1 1 1 1 1    \n' +\
        #                '1 1 1 1 1 1 1 1 \n'
        with self.assertRaises(board.MatchsticksNotConnected):
            invalid_take.remove_matches((3, 1), (3, 3))

        with self.assertRaises(board.MatchsticksNotConnected):
            invalid_take.remove_matches((5, 2), (5, 3), (5, 5))

        with self.assertRaises(board.MatchsticksNotConnected):
            invalid_take.remove_matches((5, 1), (5, 3))

        with self.assertRaises(board.MatchsticksNotConnected):
            invalid_take.remove_matches((5, 3), (5, 5))

        with self.assertRaises(board.MatchsticksNotConnected):
            invalid_take.remove_matches((5, 5), (5, 3))

    def test_remove_already_taken_matches(self):
        invalid_take = board.MatchsticksGameBoard(1)
        # board_should = '       1        \n' +\
        #                '      1 1       \n' +\
        #                '     1 1 1      \n' +\
        #                '   1 1 1 1 1    \n' +\
        #                '1 1 1 1 1 1 1 1 \n'

        invalid_take.remove_matches((1, 1))
        with self.assertRaises(board.ItemNonExistent):
            invalid_take.remove_matches((1, 1))

        invalid_take.remove_matches((3, 3))
        with self.assertRaises(board.ItemNonExistent):
            invalid_take.remove_matches((3, 1), (3, 2), (3, 3))

    def test_index_errors(self):
        invalid_take = board.MatchsticksGameBoard(1)
        # board_should = '       1        \n' +\
        #                '      1 1       \n' +\
        #                '     1 1 1      \n' +\
        #                '   1 1 1 1 1    \n' +\
        #                '1 1 1 1 1 1 1 1 \n'
        with self.assertRaises(IndexError):
            invalid_take.remove_matches((0, 0))

        with self.assertRaises(IndexError):
            invalid_take.remove_matches((6, 2))

        with self.assertRaises(IndexError):
            invalid_take.remove_matches((-1, 1))

        with self.assertRaises(IndexError):
            invalid_take.remove_matches((1, 0))

    def test_ItemNonExistent(self):
        invalid_take = board.MatchsticksGameBoard(1)
        invalid_take.remove_matches((1, 1))
        with self.assertRaises(board.ItemNonExistent):
            invalid_take.remove_matches((1, 1))

if __name__ == '__main__':
    unittest.main()
