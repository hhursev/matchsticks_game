import unittest
import player


class MatchsticksPlayerTest(unittest.TestCase):

    def test_communication_with_json_1(self):
        test_player = player.Player(
            "hristo", "./tests/testing_files/players_stats.json")
        self.assertEqual(25, test_player.wins())

    def test_communication_with_json_2(self):
        test_player = player.Player(
            "pepa", "./tests/testing_files/players_stats.json")
        self.assertEqual(None, test_player.stats)

    def test_getting_correct_stats(self):
        test_player = player.Player(
            "hristo", "./tests/testing_files/players_stats.json")

        hristo_stats = {"georgi": [10, 8], "kamen": [15, 2]}
        self.assertEqual(hristo_stats, test_player.stats)

    def test_win_ratio_vs_others(self):
        hristo = player.Player(
            "hristo", "./tests/testing_files/players_stats.json")
        georgi = player.Player(
            "georgi", "./tests/testing_files/players_stats.json")
        self.assertEqual([10, 8], hristo.win_ratio_vs(georgi))

    def test_win_ratio_vs_others_when_none(self):
        pepa = player.Player(
            "pepa", "./tests/testing_files/players_stats.json")
        hristo = player.Player(
            "hristo", "./tests/testing_files/players_stats.json")
        self.assertEqual([0, 0], pepa.win_ratio_vs(hristo))

    def test_return_correct_wins_count(self):
        hristo = player.Player(
            "hristo", "./tests/testing_files/players_stats.json")
        self.assertEqual(25, hristo.wins())

    def test_return_correct_loses_count(self):
        hristo = player.Player(
            "hristo", "./tests/testing_files/players_stats.json")
        self.assertEqual(10, hristo.loses())

    def test_return_correct_games_count(self):
        hristo = player.Player(
            "hristo", "./tests/testing_files/players_stats.json")
        self.assertEqual(35, hristo.games_played())

    def test_eq_true(self):
        some_file = "./tests/testing_files/players_stats.json"
        hristo_1 = player.Player('hristo', some_file)
        hristo_2 = player.Player('hristo', some_file)
        self.assertTrue(hristo_1 == hristo_2)

    def test_eq_false(self):
        some_file = "./tests/testing_files/players_stats.json"
        other_file = "./tests/testing_files/p_never_met.json"
        hristo_1 = player.Player('hristo', some_file)
        hristo_2 = player.Player('hristo', other_file)
        self.assertFalse(hristo_1 == hristo_2)

    def test_print_player(self):
        other_file = "./tests/testing_files/p_never_met.json"
        hristo_1 = player.Player('hristo', other_file)
        self.assertEqual('hristo', hristo_1.__str__())

if __name__ == '__main__':
    unittest.main()
