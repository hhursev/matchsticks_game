import unittest
import AI
import board


class AITests(unittest.TestCase):

    def test_power_of_2_1(self):
        computer = AI.AI(board.MatchsticksGameBoard(1))
        self.assertTrue(computer._AI__power_of_2(4))

    def test_power_of_2_2(self):
        computer = AI.AI(board.MatchsticksGameBoard(1))
        self.assertTrue(computer._AI__power_of_2(1024))

    def test_power_of_2_3(self):
        computer = AI.AI(board.MatchsticksGameBoard(1))
        self.assertFalse(computer._AI__power_of_2(7))

    def test_power_of_2_4(self):
        computer = AI.AI(board.MatchsticksGameBoard(1))
        self.assertFalse(computer._AI__power_of_2(10))

    def test_stacks_of_matches_1(self):
        b = board.MatchsticksGameBoard(1)
        b.remove_matches((2, 1))
        b.remove_matches((3, 2), (3, 3))
        b.remove_matches((5, 4))
        # board_is = '       1        \n' +\
        #            '      0 1       \n' +\
        #            '     1 0 0      \n' +\
        #            '   1 1 1 1 1    \n' +\
        #            '1 1 1 0 1 1 1 1 \n'
        computer = AI.AI(b)
        self.assertEqual([1, 1, 1, 5, 3, 4], computer._AI__stacks_of_matches())

    def test_stacks_of_matches_2(self):
        b = board.MatchsticksGameBoard(1)
        b.remove_matches((1, 1))
        b.remove_matches((3, 3))
        # board_is = '       0        \n' +\
        #            '      1 1       \n' +\
        #            '     1 1 0      \n' +\
        #            '   1 1 1 1 1    \n' +\
        #            '1 1 1 1 1 1 1 1 \n'
        computer = AI.AI(b)
        self.assertEqual([2, 2, 5, 8], computer._AI__stacks_of_matches())

    def test_break_in_power_of_twos_1(self):
        b = board.MatchsticksGameBoard(1)
        b.remove_matches((2, 1))
        b.remove_matches((3, 2), (3, 3))
        b.remove_matches((5, 4))
        # board_is = '       1        \n' +\
        #            '      0 1       \n' +\
        #            '     1 0 0      \n' +\
        #            '   1 1 1 1 1    \n' +\
        #            '1 1 1 0 1 1 1 1 \n'
        computer = AI.AI(b)
        self.assertEqual([1, 1, 1, 4, 1, 2, 1, 4],
                         computer._AI__break_in_power_of_twos())

    def test_break_in_power_of_twos_2(self):
        b = board.MatchsticksGameBoard(1)
        b.remove_matches((1, 1))
        b.remove_matches((3, 3))
        # board_is = '       0        \n' +\
        #            '      1 1       \n' +\
        #            '     1 1 0      \n' +\
        #            '   1 1 1 1 1    \n' +\
        #            '1 1 1 1 1 1 1 1 \n'
        computer = AI.AI(b)
        self.assertEqual([2, 2, 4, 1, 8],
                         computer._AI__break_in_power_of_twos())

    def test_nim_sum_calculation_1(self):
        b = board.MatchsticksGameBoard(1)
        b.remove_matches((2, 1))
        b.remove_matches((3, 2), (3, 3))
        b.remove_matches((5, 4))
        # board_is = '       1        \n' +\
        #            '      0 1       \n' +\
        #            '     1 0 0      \n' +\
        #            '   1 1 1 1 1    \n' +\
        #            '1 1 1 0 1 1 1 1 \n'
        computer = AI.AI(b)
        self.assertEqual({1: 1, 2: 1}, computer._AI__nim_sum())

    def test_nim_sum_calculation_2(self):
        b = board.MatchsticksGameBoard(1)
        b.remove_matches((1, 1))
        b.remove_matches((3, 3))
        # board_is = '       0        \n' +\
        #            '      1 1       \n' +\
        #            '     1 1 0      \n' +\
        #            '   1 1 1 1 1    \n' +\
        #            '1 1 1 1 1 1 1 1 \n'
        computer = AI.AI(b)
        self.assertEqual({1: 1, 4: 1, 8: 1}, computer._AI__nim_sum())

    def test_nim_sum_is_zero_1(self):
        b = board.MatchsticksGameBoard(1)
        b.remove_matches((5, 1), (5, 2), (5, 3))
        computer = AI.AI(b)
        self.assertTrue(computer._AI__nim_sum_zero())

    def test_nim_sum_is_zero_2(self):
        b = board.MatchsticksGameBoard(1)
        computer = AI.AI(b)
        self.assertFalse(computer._AI__nim_sum_zero())

    def test_nim_sum_is_zero_3(self):
        b = board.MatchsticksGameBoard(1)
        b.remove_matches((5, 8), (5, 7), (5, 6))
        computer = AI.AI(b)
        self.assertTrue(computer._AI__nim_sum_zero())

    def test_with_4_stacks_of_single_matchsticks(self):
        b = board.MatchsticksGameBoard(1)
        b.remove_matches((2, 1))
        b.remove_matches((3, 1), (3, 2))
        b.remove_matches((4, 2), (4, 3), (4, 4), (4, 5))
        b.remove_matches((5, 1), (5, 2), (5, 3), (5, 4))
        b.remove_matches((5, 5), (5, 6), (5, 7), (5, 8))
        # board_is = '       1        \n' +\
        #            '      0 1       \n' +\
        #            '     0 0 1      \n' +\
        #            '   1 0 0 0 0    \n' +\
        #            '0 0 0 0 0 0 0 0 \n'
        computer = AI.AI(b)
        self.assertEqual("4 1 ", computer.make_move())

    def test_to_make_nim_sum_zero_1(self):
        b = board.MatchsticksGameBoard(1)
        # board_is = '       1        \n' +\
        #            '      1 1       \n' +\
        #            '     1 1 1      \n' +\
        #            '   1 1 1 1 1    \n' +\
        #            '1 1 1 1 1 1 1 1 \n'
        computer = AI.AI(b)
        self.assertEqual("5 1 5 2 5 3 ", computer.make_move())

    def test_to_make_nim_sum_zero_2(self):
        b = board.MatchsticksGameBoard(1)
        b.remove_matches((5, 3))
        # board_is = '       1        \n' +\
        #            '      1 1       \n' +\
        #            '     1 1 1      \n' +\
        #            '   1 1 1 1 1    \n' +\
        #            '1 1 0 1 1 1 1 1 \n'
        computer = AI.AI(b)
        self.assertEqual("5 1 5 2 ", computer.make_move())

    def test_this_count_of_matches(self):
        b = board.MatchsticksGameBoard(1)
        # board_is = '       1        \n' +\
        #            '      1 1       \n' +\
        #            '     1 1 1      \n' +\
        #            '   1 1 1 1 1    \n' +\
        #            '1 1 1 1 1 1 1 1 \n'
        comp = AI.AI(b)
        self.assertEqual((8, 0, 5), comp._AI__take_this_count_of_matches(5))

    def test_avoid_to_make_nim_sum_when_edge_cases_1(self):
        b = board.MatchsticksGameBoard(1)
        b.remove_matches((3, 1), (3, 2), (3, 3))
        b.remove_matches((4, 1), (4, 2), (4, 3), (4, 4), (4, 5))
        b.remove_matches((5, 2), (5, 3), (5, 4), (5, 5), (5, 6), (5, 7))
        # board_is = '       1        \n' +\
        #            '      1 1       \n' +\
        #            '     0 0 0      \n' +\
        #            '   0 0 0 0 0    \n' +\
        #            '1 0 0 0 0 0 0 1 \n'
        computer = AI.AI(b)
        self.assertEqual("2 1 2 2 ", computer.make_move())

    def test_avoid_to_make_nim_sum_when_edge_cases_2(self):
        b = board.MatchsticksGameBoard(1)
        b.remove_matches((3, 2), (3, 3))
        b.remove_matches((4, 1), (4, 2), (4, 3), (4, 4), (4, 5))
        b.remove_matches((5, 2), (5, 3), (5, 4), (5, 5), (5, 6), (5, 7))
        # board_is = '       1        \n' +\
        #            '      1 1       \n' +\
        #            '     1 0 0      \n' +\
        #            '   0 0 0 0 0    \n' +\
        #            '1 0 0 0 0 0 0 1 \n'
        computer = AI.AI(b)
        self.assertEqual("2 1 ", computer.make_move())

    def test_AI_decisions_on_type_9_test1(self):
        b = board.MatchsticksGameBoard(9)
        # board_is = '    1     \n' +\
        #            '  1 1 1   \n' +\
        #            '1 1 1 1 1 \n' +\
        #            ' 1 1 1 1  \n'
        computer = AI.AI(b)
        self.assertEqual("2 1 2 2 2 3 ", computer.make_move())

    def test_move_to_str(self):
        b = board.MatchsticksGameBoard(1)
        computer = AI.AI(b)
        self.assertEqual("2 1 2 2 ", computer._AI__move_to_str((2, 0, 2)))

    def test_max_stacks_on_row(self):
        b = board.MatchsticksGameBoard(1)
        computer = AI.AI(b)
        row = [1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1]
        self.assertEqual(6, computer._AI__max_stack_on_row(row))

    def test_print(self):
        b = board.MatchsticksGameBoard(1)
        computer = AI.AI(b)
        self.assertEqual('computer', computer.__str__())

    def test_only_single_matches(self):
        b = board.MatchsticksGameBoard(9)
        # board_is = '    1     \n' +\
        #            '  1 1 1   \n' +\
        #            '1 1 1 1 1 \n' +\
        #            ' 1 1 1 1  \n'
        b.remove_matches((2, 2))
        b.remove_matches((3, 2))
        b.remove_matches((3, 4))
        b.remove_matches((4, 1), (4, 2), (4, 3))
        computer = AI.AI(b)
        self.assertTrue(computer._AI__only_single_matches())

    def test_only_signles_plus_big_odd(self):
        b = board.MatchsticksGameBoard(9)
        # board_is = '    1     \n' +\
        #            '  1 1 1   \n' +\
        #            '1 1 1 1 1 \n' +\
        #            ' 1 1 1 1  \n'
        b.remove_matches((2, 2))
        b.remove_matches((3, 2))
        b.remove_matches((3, 4))
        computer = AI.AI(b)
        self.assertTrue(computer._AI__stacks_with_singles_and_big_odd())

    def test_only_signles_plus_big_even(self):
        b = board.MatchsticksGameBoard(9)
        # board_is = '    1     \n' +\
        #            '  1 1 1   \n' +\
        #            '1 1 1 1 1 \n' +\
        #            ' 1 1 1 1  \n'
        b.remove_matches((3, 2))
        b.remove_matches((3, 4))
        b.remove_matches((4, 1))
        b.remove_matches((4, 3), (4, 4))
        computer = AI.AI(b)
        self.assertTrue(computer._AI__stacks_with_singles_and_big_even())


if __name__ == '__main__':
    unittest.main()
